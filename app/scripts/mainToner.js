"use strict"

$(document).ready(function() {
    //animacoes
    var wow = new WOW({
        boxClass:     'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       0,          // distance to the element when triggering the animation (default is 0)
        mobile:       true,       // trigger animations on mobile devices (default is true)
        live:         true,       // act on asynchronously loaded content (default is true)
        scrollContainer: null // optional scroll container selector, otherwise use window
    });

    wow.init();

    //change video
    $('.videos .item').click(function(){
        $('.videos .item').removeClass('active');
        $(this).addClass('active');

        var cod = $(this).attr('data-href');
        var url = $('.reinventou iframe').attr('src').split('/embed')[0];
        url  += '/embed/' + cod;
        $('.reinventou iframe').attr('src', url);
        return false;
    });

    // $(".reinventou .videos").slick({        
    //     infinite: true,
    //     slidesToShow: 5
    // });
});
