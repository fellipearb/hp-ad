"use strict"

$(document).ready(function() {
    //animacoes
    var wow = new WOW({
        boxClass:     'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       0,          // distance to the element when triggering the animation (default is 0)
        mobile:       true,       // trigger animations on mobile devices (default is true)
        live:         true,       // act on asynchronously loaded content (default is true)
        scrollContainer: null // optional scroll container selector, otherwise use window
    });

    wow.init();

    //change video
    $('.hpexplica .container .option a').click(function(){
        var cod = $(this).attr('href');
        var url = $('.video iframe').attr('src').split('/embed')[0];
        url  += '/embed/' + cod;
        $('.video iframe').attr('src', url);
        return false;
    });
});